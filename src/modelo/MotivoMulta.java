package modelo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author firofenix1986
 */
public class MotivoMulta {

    private short codigo;
    private String descripcion;
    private int valor;

    public MotivoMulta(short codigo, String descripcion, int valor)
            throws Exception {
        if (1000 > codigo || codigo > 1020) {
            throw new Exception("Ingrese un codigo de 4 digitos\n");
        }

        if (descripcion == null || "".equals(descripcion.trim())) {
            throw new Exception("La descripcion del motivo no puede "
                    + "estar vacia");
        }

        if (valor <= 0) {
            throw new Exception("El valor de la multa debe ser  "
                    + "mayor a 0");
        }

        this.codigo = codigo;
        this.descripcion = descripcion;
        this.valor = valor;
    }

    public short getCodigo() {
        return this.codigo;
    }

    public String getDescripcion() {
        return this.descripcion;

    }

    public int getValor() {
        return this.valor;
    }

    public void setCodigo(short codigo) throws Exception {
        if (1000 > codigo || codigo > 1020) {
            throw new Exception("Ingrese un codigo de 4 digitos\n");

        }

        this.codigo = codigo;
    }

    public void setDescripcion(String descripcion) throws Exception {
        if (descripcion == null || "".equals(descripcion.trim())) {
            throw new Exception("La descripcion del motivo no puede "
                    + "estar vacia");
        }
        this.descripcion = descripcion;
    }

    public void setValor(int valor) throws Exception {
        if (valor <= 0) {
            throw new Exception("El valor de la multa debe ser  "
                    + "mayor a 0");
        }
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "MotivoMulta{" + "codigo=" + codigo + ", descripcion=" + descripcion + ", valor=" + valor + '}';
    }

}
