package modelo;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author firofenix1986
 */
public class Multa {

    private Persona person;
    private Vehiculo car;
    private Multa fine;
    private Agente agent;
    private Date fechaMulta;
    private List<MotivoMulta> due = new LinkedList<>();

    public Multa(Persona person, Vehiculo car, Multa fine,
            Agente agent, Date fechaMulta) {
        this.person = person;
        this.car = car;
        this.fine = fine;
        this.agent = agent;
        this.fechaMulta = fechaMulta;
    }

    public Persona getPerson() {
        return person;
    }

    public Vehiculo getCar() {
        return car;
    }

    public Multa getFine() {
        return fine;
    }

    public Agente getAgent() {
        return agent;
    }

    public Date getFechaMulta() {
        return fechaMulta;
    }

    public List<MotivoMulta> getDue() {
        return due;
    }

    @Override
    public String toString() {
        return "Multa{" + "person=" + person + ", car=" + car + ", fine=" + fine + ", agent=" + agent + ", fechaMulta=" + fechaMulta + ", due=" + due + '}';
    }

}
