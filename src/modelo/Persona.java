package modelo;

import java.util.regex.Pattern;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author firofenix1986
 */
public class Persona {

    private long identificacion;
    private String nombre;
    private String apellido;

    public Persona(long identificacion, String nombre, String apellido)
            throws Exception {

        setIdentificacion(identificacion);
        setNombre(nombre);
        setApellido(apellido);
    }

    public long getIdentificacion() {
        return identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setIdentificacion(long identificacion) throws Exception {
        if ((identificacion + "").length() < 7 || (identificacion + "").length() > 10) {
            throw new Exception("\n La identificaciòn de la persona "
                    + "debe contener entre 7 y 10 digitos\n");

        }
        this.identificacion = identificacion;
    }

    public void setNombre(String nombre) throws Exception {
        if (nombre == null
                || "".equals(nombre.trim())) {
            throw new Exception(
                    "El nombre de la persona no puede estar vacio");

        }
        this.nombre = nombre;
    }

    public void setApellido(String apellido) throws Exception {
        if (apellido == null
                || "".equals(apellido.trim())) {
            throw new Exception(
                    "El apellido de la persona no puede estar vacio");

        }
        this.apellido = apellido;
    }

    @Override
    public String toString() {
        return "Persona{" + "identificacion=" + identificacion + ", nombre=" + nombre + ", apellido=" + apellido + '}';
    }

}
