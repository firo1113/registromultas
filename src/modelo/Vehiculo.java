package modelo;

import java.util.regex.Pattern;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author firofenix1986
 */
public class Vehiculo {

    private String placa;
    private String marca;
    private short modelo;
    private String color;

    public Vehiculo(String placa, String marca, short modelo, String color) throws Exception {
        setPlaca(placa);
        setMarca(marca);
        setModelo(modelo);
        setColor(color);

    }

    public String getPlaca() throws Exception {

        return placa;
    }
//    
//    public String getPlaca() {
//        return placa;
//    }

    public String getMarca() {
        return marca;
    }

    public short getModelo() {
        return modelo;
    }

    public String getColor() {
        return color;
    }

    public void setPlaca(String placa) throws Exception {
        if (placa == null || "".equals(placa.trim())) {

            throw new Exception(
                    "El numero de la placa esta vacio, ingreselo");

        }
        if (placa.length() != 6) {
            throw new Exception("El numero de la placa debe contener"
                    + " " + "6 caracteres\n");
        }
        this.placa = placa;
    }

    public void setMarca(String marca) throws Exception {
        if (marca == null
                || "".equals(marca.trim())) {
            throw new Exception(
                    "la marca del vehiculo esta vacia , ingresela");

        }
        this.marca = marca;
    }

    public void setModelo(short modelo) throws Exception {
        if (1990 > modelo || modelo > 9999) {
            throw new Exception(
                    "Ingrese el año de modelo a partir del año 1990");
        }
        if (modelo == 0 || "".equals(Short.toString(modelo).trim())) {
            throw new Exception("el modelo no puede estar vacio");

        }
        this.modelo = modelo;
    }

    public void setColor(String color) throws Exception {
        if (color == null
                || "".equals(color.trim())) {
            throw new Exception(
                    "El color del vehiculo esta vacio, ingresela");

        }
        this.color = color;
    }

    @Override
    public String toString() {
        return "Vehiculo{" + "placa=" + placa + ", modelo=" + modelo + ", color=" + color + ", marca=" + marca + '}';
    }

}
