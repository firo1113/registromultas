package modelo;

import java.time.LocalDate;
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author firofenix1986
 */
public class Licencia {

    private Persona driver;
    private Date fechaExpedicion;
    private Categoria type;

    public Licencia(Persona driver, Date fechaExpedicion, Categoria type) {
        this.driver = driver;
        this.fechaExpedicion = fechaExpedicion;
        this.type = type;
    }

    public Persona getDriver() {
        return driver;
    }

    public Date getFechaExpedicion() {
        return fechaExpedicion;
    }

    public Categoria getType() {
        return type;

    }

    public void setDriver(Persona driver) {
        this.driver = driver;
    }

    public void setFechaExpedicion(Date fechaExpedicion) {
        this.fechaExpedicion = fechaExpedicion;
    }

    public void setType(Categoria type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Licencia{" + "driver=" + driver + ", fechaExpedicion=" + fechaExpedicion + ", type=" + type + '}';
    }

//    public void getFechaExpedicion(Date fExpedicion) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }

}
